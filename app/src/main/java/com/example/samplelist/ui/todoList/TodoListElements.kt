package com.example.samplelist.ui.todoList

import com.example.samplelist.data.network.entity.RestError
import com.example.samplelist.data.network.entity.TodoEntity
import com.example.samplelist.ui.tools.BaseUiModel
import com.example.samplelist.ui.tools.BaseViewModelEvent

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 10:10
 * @summary : TodoListElements
 */

sealed class TodoListViewModelEvent : BaseViewModelEvent {
    object RequestTodoList : TodoListViewModelEvent()
}

sealed class TodoListUiModel : BaseUiModel {
    data class ResponseTodoListSuccess(val list: List<TodoEntity>) : TodoListUiModel()
    data class ResponseThrowableError(val t: Throwable) : TodoListUiModel()
    data class ResponseRestError(val restError: RestError?) : TodoListUiModel()
    object Loading : TodoListUiModel()
}