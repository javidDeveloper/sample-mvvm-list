package com.example.samplelist.ui.tools

import androidx.lifecycle.ViewModel
/**
 * @author  : Javid
 * @summary : this class a generic ViewModel
 * [BaseViewModelEvent] is events pass from UI to viewModel and [BaseUiModel]
 * response or event from viewModel to UI
 */

abstract class BaseViewModel<UIM : BaseUiModel,in VME : BaseViewModelEvent> : ViewModel() {
    var uiModelNotifier = MultipleLiveData<UIM>()

    /**
     * this fun used in view model for handle event from ui
     */
    abstract fun onEvent(useCase:VME)
}