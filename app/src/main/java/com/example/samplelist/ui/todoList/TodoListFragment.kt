package com.example.samplelist.ui.todoList

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.samplelist.data.network.entity.RestError
import com.example.samplelist.data.network.entity.TodoEntity
import com.example.samplelist.databinding.FragmentTodoListBinding
import com.example.samplelist.ui.tools.hide
import com.example.samplelist.ui.tools.show
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class TodoListFragment : Fragment() {
    private val vm: TodoListVM by viewModels()
    private lateinit var binding: FragmentTodoListBinding
    private val todoListAdapter by lazy {
        TodoListAdapter {
            showTodoItemDialog(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTodoListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {

            vm.onEvent(TodoListViewModelEvent.RequestTodoList)

            rlcTodoList.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = todoListAdapter
            }

            tilSearch.setEndIconOnClickListener {
                acSearch.text = null
                todoListAdapter.filter.filter(null)
            }
            acSearch.doAfterTextChanged {
                todoListAdapter.filter.filter(it)
            }

            vm.uiModelNotifier.observe(viewLifecycleOwner) {
                when (it) {
                    TodoListUiModel.Loading -> {
                        binding.txtErrorMsg.hide()
                        progress.show()
                    }
                    is TodoListUiModel.ResponseRestError -> {
                        responseRestError(it.restError)
                    }
                    is TodoListUiModel.ResponseThrowableError -> {
                        responseThrowableError(it.t)
                    }
                    is TodoListUiModel.ResponseTodoListSuccess -> {
                        responseTodoListSuccess(it.list)
                    }
                }
            }
        }
    }

    private fun responseTodoListSuccess(list: List<TodoEntity>) {
        binding.progress.hide()
        binding.txtErrorMsg.hide()
        binding.tilSearch.isEnabled = true
        todoListAdapter.setList(list)
    }

    private fun responseThrowableError(t: Throwable) {
        binding.progress.hide()
        binding.txtErrorMsg.show()
    }

    private fun responseRestError(restError: RestError?) {
        binding.progress.hide()
        binding.txtErrorMsg.show()
        binding.txtErrorMsg.text = restError?.message
    }

    private fun showTodoItemDialog(todoEntity: TodoEntity) {
        AlertDialog.Builder(context)
            .setMessage("Todo Id  is : ${todoEntity.id}")
            .setPositiveButton(
                "ok"
            ) { dialog, id ->
                dialog.dismiss()
            }
            .create()
            .show()
    }
}