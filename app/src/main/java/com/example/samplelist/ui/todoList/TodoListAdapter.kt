package com.example.samplelist.ui.todoList

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.samplelist.data.network.entity.TodoEntity
import com.example.samplelist.databinding.TodoListItemBinding
import java.util.*

/**
 * @author  : Javid
 * @summary : ---
 */

class TodoListAdapter(private val itemClick: (TodoEntity) -> Unit) :
    RecyclerView.Adapter<TodoListAdapter.ViewHolder>(), Filterable {
    private var list = listOf<TodoEntity>()

    fun setList(list: List<TodoEntity>) {
        this.list = list.toMutableList()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: TodoListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: TodoEntity) {
            binding.txtTitle.text = model.title
            binding.txtBody.text = model.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = TodoListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        list[position].also { resultsEntity ->
            holder.bind(resultsEntity)
            holder.itemView.setOnClickListener {
                itemClick.invoke(resultsEntity)
            }
        }
    }

    override fun getItemCount(): Int = list.size


    override fun getFilter(): Filter =
        customFilter

    private val customFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredList = mutableListOf<TodoEntity>()
            if (constraint == null || constraint.isEmpty()) {
                filteredList.addAll(list)
            } else {
                for (item in list) {
                    if (item.title.lowercase(Locale.getDefault()).startsWith(
                            constraint.toString().lowercase(Locale.getDefault())
                        )
                    ) {
                        filteredList.add(item)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence?, filterResults: FilterResults?) {
            list = filterResults?.values as List<TodoEntity>
            notifyDataSetChanged()
        }
    }
}