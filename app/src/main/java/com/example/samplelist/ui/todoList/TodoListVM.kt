package com.example.samplelist.ui.todoList


import androidx.lifecycle.viewModelScope
import com.example.samplelist.data.network.DataState
import com.example.samplelist.data.repository.TodoRepository
import com.example.samplelist.di.Repository
import com.example.samplelist.ui.tools.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 09:48
 * @summary : TodoListVM
 */
@ExperimentalCoroutinesApi
class TodoListVM(
    private val todoRepository: TodoRepository = Repository.todoRepository
) : BaseViewModel<TodoListUiModel, TodoListViewModelEvent>() {
    override fun onEvent(useCase: TodoListViewModelEvent) {
        when (useCase) {
            TodoListViewModelEvent.RequestTodoList -> requestTodoList()
        }
    }

    private fun requestTodoList() {
        viewModelScope.launch {
            todoRepository.requestTodoList().collectLatest {
                when (it) {
                    is DataState.ApiRestError -> {
                        uiModelNotifier.postValue(
                            TodoListUiModel.ResponseRestError(it.restError)
                        )
                    }
                    is DataState.ApiSuccess -> {
                        uiModelNotifier.postValue(
                            TodoListUiModel.ResponseTodoListSuccess(it.data)
                        )
                    }
                    is DataState.ApiThrowableError -> {
                        uiModelNotifier.postValue(
                            TodoListUiModel.ResponseThrowableError(it.t)
                        )
                    }
                    DataState.Loading -> {
                        uiModelNotifier.postValue(
                            TodoListUiModel.Loading
                        )
                    }
                    DataState.NetworkError -> {}
                }
            }
        }
    }
}