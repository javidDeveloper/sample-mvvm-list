package com.example.samplelist

import android.app.Application
import com.example.samplelist.di.Network

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 08:58
 * @summary : TodoApp
 */

class TodoApp :Application() {

    override fun onCreate() {
        super.onCreate()
        Network
    }
}