package com.example.samplelist.di

import com.example.samplelist.BuildConfig
import com.example.samplelist.data.network.ApiService
import com.example.samplelist.data.repository.TodoRepository
import com.example.samplelist.data.repository.TodoRepositoryImpl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Network {
    private const val baseUrl = BuildConfig.TODO_APP_BASE_URL
    val apiService: ApiService by lazy {
        provideApiService(
            provideRetrofit(
                provideGsonBuilder()
            )
        )
    }

    private fun provideGsonBuilder(): Gson {
        return GsonBuilder().setLenient()
            .create()
    }

    private fun provideRetrofit(
        gson: Gson,
    ): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    private fun provideApiService(retrofit: Retrofit.Builder): ApiService {
        return retrofit
            .build()
            .create(ApiService::class.java)
    }
}


@ExperimentalCoroutinesApi
object Repository {
    val todoRepository: TodoRepository by lazy {
        TodoRepositoryImpl(Network.apiService)
    }

    private fun provideRepository(
        network: Network,
    ): TodoRepository =
        TodoRepositoryImpl(network.apiService)
}