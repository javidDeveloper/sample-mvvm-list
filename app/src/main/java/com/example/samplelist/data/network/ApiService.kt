package com.example.samplelist.data.network

import com.example.samplelist.data.network.entity.TodoEntity
import retrofit2.Call
import retrofit2.http.*

/**
 * @author  : Javid
 * @since   : 9/23/2021 -- 00:08
 * @summary : ---
 */

interface ApiService {

    @GET("posts")
    fun requestTodoList(): Call<List<TodoEntity>>

}