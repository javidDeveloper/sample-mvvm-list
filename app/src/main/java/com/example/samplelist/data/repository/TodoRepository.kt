package com.example.samplelist.data.repository

import com.example.samplelist.data.network.DataState
import com.example.samplelist.data.network.entity.TodoEntity
import kotlinx.coroutines.flow.Flow

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 08:35
 * @summary : TodoRepository
 */

interface TodoRepository {

    /**
     * this fun request `todoList` from server
     * @return listOf[TodoEntity]
     */
    fun requestTodoList(): Flow<DataState<List<TodoEntity>>>
}