package com.example.samplelist.data.network.entity

import com.google.gson.annotations.SerializedName

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 09:40
 * @summary : RestError
 */

data class RestError(
    @SerializedName("code") val code: Int,
    @SerializedName("message") val message: String,
    @SerializedName("UUID") val UUID: String?,
)