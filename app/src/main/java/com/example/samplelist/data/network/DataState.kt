package com.example.samplelist.data.network

import com.example.samplelist.data.network.entity.RestError

/**
 * @author  : Javid
 * @summary : DataState
 */

sealed class DataState<out R> {
    object Loading : DataState<Nothing>()
    data class ApiSuccess<out R>(val data: R) : DataState<R>()
    data class ApiRestError(val restError: RestError?) : DataState<Nothing>()
    data class ApiThrowableError(val t: Throwable) : DataState<Nothing>()
    object NetworkError : DataState<Nothing>()
}