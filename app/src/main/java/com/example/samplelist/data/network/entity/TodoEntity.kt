package com.example.samplelist.data.network.entity

import com.google.gson.annotations.SerializedName

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 08:38
 * @summary : TodoEntity
 */

data class TodoEntity(
    @SerializedName("userId") val userId: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String,
)
