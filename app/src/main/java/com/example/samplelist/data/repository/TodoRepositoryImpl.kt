package com.example.samplelist.data.repository

import com.example.samplelist.data.network.ApiService
import com.example.samplelist.data.network.DataState
import com.example.samplelist.data.network.entity.RestError
import com.example.samplelist.data.network.entity.TodoEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 08:36
 * @summary : TodoRepositoryImpl
 */

@ExperimentalCoroutinesApi
class TodoRepositoryImpl(
    private val apiService: ApiService
) : TodoRepository {
    override fun requestTodoList(): Flow<DataState<List<TodoEntity>>> =
        callbackFlow {
            trySend(DataState.Loading)
            apiService.requestTodoList().enqueue(object : Callback<List<TodoEntity>> {
                override fun onResponse(
                    call: Call<List<TodoEntity>>,
                    response: Response<List<TodoEntity>>
                ) {
                    if (response.isSuccessful)
                        response.body()?.let {
                            trySend(DataState.ApiSuccess(it))
                        }
                    else {
                        trySend(
                            DataState.ApiRestError(
                                RestError(
                                    code = response.code(),
                                    message = response.errorBody().toString(),
                                    UUID = null
                                )
                            )
                        )
                    }
                }

                override fun onFailure(call: Call<List<TodoEntity>>, t: Throwable) {
                    trySend(DataState.ApiThrowableError(t))
                }
            })

            awaitClose { cancel() }
        }
}
