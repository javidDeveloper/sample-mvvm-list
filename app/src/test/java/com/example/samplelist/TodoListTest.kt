package com.example.samplelist

import com.example.samplelist.data.network.ApiService
import kotlinx.coroutines.runBlocking
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.await
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author  : Javid
 * @since   : 2022/04/20 -- 11:40
 * @summary : TodoListTest
 */

class TodoListTest {

    private lateinit var service: ApiService
    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(server.url(BuildConfig.TODO_APP_BASE_URL))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Test
    fun getSearchedResult_sentRequest_receivedExpected() {
        runBlocking {
            //Send Request to the MockServer
            val responseBody = service.requestTodoList().await()
            //Request received by the mock server
            assert(responseBody.isNotEmpty())
            assert(responseBody.size == 100)
        }
    }


    @After
    fun tearDown() {
        server.shutdown()
    }
}